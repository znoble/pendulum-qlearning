#!/usr/bin/env python3

import gym
from random import choice
import math

def randomchoice():
    return choice([0,1])

def physics(th, sig):
    if th < 0 and sig < 0:
        return 0
    elif th > 0 and sig > 0:
        return 1
    else:
        return choice([0,1])

if __name__ == "__main__":
    env = gym.make('Pendulum-v0')

    niter = 0
    totalmoves = 0
    while True:
        x1 = env.reset()              # game setup
        x2 = 1.0
        steps = 0
        while True:                   # game begins
            steps += 1
            env.render()              # render game
            y = env.action_space.sample()
            print("action:", y)
            x1, x2, done, x3 = env.step(y)                # make move and get feedback
            print(x1,x2, done, x3)
            if done:                                      # gameover, score == num moves
                totalmoves += steps
                niter += 1
                print(f"{steps:3d} {totalmoves/niter:9.3f}")
                print(x1)
                input()
                break
    env.close()
