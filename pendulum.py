#!/usr/bin/env python3

import sys
import gym
import numpy as np
from random import choice, random, shuffle
import math
import matplotlib.pyplot as plt

MAXSTEP = 200
#Goal is to minimize the number of steps needed to get the pendulum vertical

class Agent:

    def __init__(self, ntrain=10000, ntest=100, render=False):
        self.env = gym.make('Pendulum-v0')        
        self.render = render
        self.alpha    = 1.0     # learning rate
        self.minalpha = 0.05
        self.decay    = 575.0
        self.gamma    = 0.90    # discount rate
        self.epsilon  = 0.99    # exploration rate
        self.minepsilon = 0.1

        self.ntrain = ntrain
        self.trainIters = []
        self.ntest = ntest
        self.testIters = []

        self.buckets = [21,21,21]                               # need to set buckets to different sizes?
        self.low = self.env.observation_space.low[2] - 0.1
        self.high = self.env.observation_space.high[2] + 0.1
        self.bucket_ranges = [[-1,1], [-1,1], [self.low,self.high]]  
        self.bucket_endpoints = {}
        self.createbuckets()

        self.actions = list(range(-2,3))  # push left, push a lil left, don't push, push a lil right, push right
        self.qtable = {(a,b,c): [0]*len(self.actions) for a in range(self.buckets[0]) for b in range(self.buckets[1]) for c in range(self.buckets[2])}
        self.visits = {(a,b,c): [0]*len(self.actions) for a in range(self.buckets[0]) for b in range(self.buckets[1]) for c in range(self.buckets[2])}



    def createbuckets(self):                # allow buckets to be different sizes
        for i in range(len(self.buckets)):
            ranges = []
            r = self.bucket_ranges[i]
            n = self.buckets[i]
            totallen = sum([abs(x) for x in r])
            bucketlen = totallen / n
            p1 = r[0]
            p2 = p1 + bucketlen
            ranges.append((p1, p2))
            for j in range(1, n):
                p1 = p2
                p2 += bucketlen
                ranges.append((p1, p2))
            self.bucket_endpoints[i] = ranges  

    def discretizestate(self, state):
        disc_state = []
        for i in range(len(state)):
            bucket_ranges = self.bucket_endpoints[i]
            for j in range(len(bucket_ranges)):
                if bucket_ranges[j][0] <= state[i] < bucket_ranges[j][1]:
                    disc_state.append(j)
                    break
        if len(disc_state) < len(self.buckets):
            print("State value out of bounds.")
            print(disc_state, state)
            sys.exit()
        return tuple(disc_state)

    def discState(self, state):
        disc_state = []
        for i in range(len(state)):
            scalefactor = state[i] / self.high - self.high / self.low
            scaled = state[i] * scalefactor
            scaled = int(scaled)
            disc_state.append(scaled)
        return tuple(disc_state)


    def train(self):
        for i in range(self.ntrain):
            step = 0
            stepsAtVert = 0
            state = self.env.reset()
            done = False
            while not done:
                step += 1
                oldstate = self.discretizestate(state)
                move = self.getmove(oldstate)
                state, reward, done, info = self.env.step([move])   
                newstate = self.discretizestate(state)
                self.feedback(newstate, oldstate, move, reward, i)
                #only used to for ploting graph
                c = state[0]
                s = state[1]
                if (-1.1 <= c <= 1.1 and -0.1 < s < 0.1):
                    stepsAtVert += 1
            self.trainIters.append(stepsAtVert)

    def run(self):
        self.epsilon = 0.0
        for i in range(self.ntest):
            step = 0
            stepsAtVert = 0
            state = self.env.reset()
            done = False
            while not done:
                step += 1
                if self.render:
                    self.env.render()
                oldstate = self.discretizestate(state)
                move = self.getmove(oldstate)
                state, reward, done, _ = self.env.step([move])
                #only used to for ploting graph
                c = state[0]
                s = state[1]
                if (-1.1 <= c <= 1.1 and -0.1 < s < 0.1):
                    stepsAtVert += 1
            self.testIters.append(stepsAtVert)
            if self.render:
                print(stepsAtVert)
                input()
        avg_step = sum(self.testIters) / len(self.testIters)
        print(avg_step)


    def getmove(self, state):            
        crit = random()
        if crit < self.epsilon:          #exploration route
            direc = min(self.actions, key=lambda x: self.visits[state][x])
        else:                            # exploitation route
            direc = max(self.actions, key=lambda x: self.qtable[state][x])
        return direc  

    def feedback(self, newstate, oldstate, move, reward, trialnum):
        # this method updates qtable based on bellman's equation
        self.visits[newstate][move] += 1
        chn = self.alpha * (reward + self.gamma * self.qtable[newstate][move])
        self.qtable[oldstate][move] = chn + (1-self.alpha)*self.qtable[oldstate][move]
        self.alpha = max(self.minalpha, min(1.0, math.exp(-trialnum/self.decay)))
        self.epsilon = max(self.minepsilon, min(.99, math.exp(-trialnum/self.decay)))

    def close(self):
        self.env.close()    
       
    def dumpVisits(self):
        print("\n Visits Table ")
        numVisited = sum([1 for x in self.visits if sum(self.visits[x]) > 0])
        print(numVisited)
        sortedVisits = sorted(self.visits, key=lambda x:self.visits[x], reverse=True)
        for state in sortedVisits:
            if sum(self.visits[state]) == 0:
                continue
            print("{0:5s} {1:10s}".format(str(state), str(sum(self.visits[state]))))
        print()


    def showPlot(self):
        data = np.array(self.trainIters)
        print(data.mean(), data.std())
        plt.scatter(range(len(data)), data)
        plt.show() 


if __name__ == "__main__":

    if len(sys.argv) > 1: 
        render = True
    else:
        render = False
    agent = Agent(render=render)
    agent.train()
    agent.showPlot()
    agent.run()
    #agent.dumpVisits()
    agent.close()        
